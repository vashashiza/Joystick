﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Events;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ButtonProvaider))]
public class Joystick : MonoBehaviour
{
    public float MaxDistance;

    private bool isUse;

    private Vector3 test;

    [SerializeField]
    private ButtonProvaider buttonProvaider;
    [SerializeField]
    private Transform joystickImage;

    protected virtual void Awake()
    {
        if (!joystickImage) joystickImage = transform.Find("Image");
        if (!buttonProvaider) buttonProvaider = GetComponent<ButtonProvaider>();

        buttonProvaider.PointerDown += ButtonProvaider_PointerDown;
        buttonProvaider.PointerUp += ButtonProvaider_PointerUp;

    }

    private void ButtonProvaider_PointerUp(object sender, PointerEventArg e)
    {
        isUse = false;
    }

    private void ButtonProvaider_PointerDown(object sender, PointerEventArg e)
    {
        isUse = true;
    }

    [SerializeField]
    private float forceX;
    public float ForceX
    {
        protected get
        {
            return forceX;
        }

        set
        {
            forceX = value;
        }
    }
    [SerializeField]
    private float forceY;
    public float ForceY
    {
        protected get
        {
            return forceY;
        }

        set
        {
            forceY = value;
        }
    }

    [SerializeField]
    private Canvas canvas;

    public void Update()
    {
        RectTransform CanvasRect = canvas.GetComponent<RectTransform>();
       
        MaxDistance = (GetComponent<RectTransform>().rect.height)/2;
       

        if (isUse)
        {
            if (Vector3.Distance(transform.position, Input.mousePosition) < MaxDistance)
            {
                joystickImage.position = Input.mousePosition;
            }
            else
            {
                test = Input.mousePosition - transform.position;
                joystickImage.position = transform.position + (test.normalized * MaxDistance);
            }
            forceX = joystickImage.localPosition.x / MaxDistance;
            forceY = joystickImage.localPosition.y / MaxDistance;
        }
        else
        {
            joystickImage.transform.localPosition = new Vector3();
            forceX = 0;
            forceY = 0;
        }
        
    }
}


/*
 
     using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.Events;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ButtonProvaider))]
public class Joystick : MonoBehaviour
{
    public float MaxDistance;

    private bool isUse;

    private Vector3 test;

    [SerializeField]
    private ButtonProvaider buttonProvaider;
    [SerializeField]
    private Transform joystickImage;

    protected virtual void Awake()
    {
        if (!joystickImage) joystickImage = transform.Find("Image");
        if (!buttonProvaider) buttonProvaider = GetComponent<ButtonProvaider>();

        buttonProvaider.PointerDown += ButtonProvaider_PointerDown;
        buttonProvaider.PointerUp += ButtonProvaider_PointerUp;

    }

    private void ButtonProvaider_PointerUp(object sender, PointerEventArg e)
    {
        isUse = false;
    }

    private void ButtonProvaider_PointerDown(object sender, PointerEventArg e)
    {
        isUse = true;
    }

    [SerializeField]
    private float forceX;
    public float ForceX
    {
        protected get
        {
            return forceX;
        }

        set
        {
            forceX = value;
        }
    }
    [SerializeField]
    private float forceY;
    public float ForceY
    {
        protected get
        {
            return forceY;
        }

        set
        {
            forceY = value;
        }
    }

    [SerializeField]
    private Canvas canvas;

    public void Update()
    {
        RectTransform CanvasRect = canvas.GetComponent<RectTransform>();
       
        MaxDistance = (GetComponent<RectTransform>().rect.height)/2;
       

        if (isUse)
        {
            if (Vector3.Distance(transform.position, Input.mousePosition) < MaxDistance)
            {
                joystickImage.position = Input.mousePosition;
            }
            else
            {
                test = Input.mousePosition - transform.position;
                joystickImage.position = transform.position + (test.normalized * MaxDistance);
            }
            forceX = joystickImage.localPosition.x / MaxDistance;
            forceY = joystickImage.localPosition.y / MaxDistance;
        }
        else
        {
            joystickImage.transform.localPosition = new Vector3();
            forceX = 0;
            forceY = 0;
        }
        
    }
}




     */
