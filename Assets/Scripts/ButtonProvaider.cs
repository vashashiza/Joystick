﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonProvaider : Button
{
    public event EventHandler<PointerEventArg> PointerDown;
    public event EventHandler<PointerEventArg> PointerUp;

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (PointerDown != null) PointerDown(this, new PointerEventArg(eventData));
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (PointerUp != null) PointerUp(this, new PointerEventArg(eventData));
    }
}

public class PointerEventArg : EventArgs
{
    public PointerEventData Data;

    public PointerEventArg()
    {

    }

    public PointerEventArg(PointerEventData data)
    {
        Data = data;
    }
}